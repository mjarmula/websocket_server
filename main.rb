$LOAD_PATH << './server'
require 'yaml'
require 'pry'
require 'websocket_server'

config = YAML.load_file('config/websocket_server.yml')
server = Server::WebsocketServer.new(config)
server.start
